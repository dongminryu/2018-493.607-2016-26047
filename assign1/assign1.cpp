//============================================================================
// Name        : assign1.cpp
// Author      : Dongmin Ryu
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <algorithm>
#include <map>
#include <iterator>

using namespace std;

map<string, int> m;

void comma(string line);
void apostrophe(string line);
void others(string line);

/** save words to the map **/
void savetobuffer(string &word, map<string, int> &mymap){
	mymap[word] +=1;
}

/** to sort to the descending order, string - integer pair should be flipped over**/
template <typename A, typename B>
pair<B, A> flippair(const pair<A, B> &wordpair){
	return pair<B, A>(wordpair.second, wordpair.first);
}

/** multimap is used to sort **/
template <typename A, typename B>
multimap<B, A> flipmap(const map<A, B> &wordmap){
	multimap<B, A> forsort;
	transform(wordmap.begin(), wordmap.end(), inserter(forsort, forsort.begin()), flippair<A, B>);
	return forsort;
}

/** sort and print **/
void print(multimap<int, string> &forsort){
	for (auto i=forsort.rbegin(); i!=forsort.rend(); ++i){
		cout << i->second << ": " << i->first << '\n';
	}
}

/** tokenize comma and do exception process**/
void comma(string line){

	char *buffer[1000] = {NULL, };
	string sbuffer[1000] = {};
	string line2="";

	/** changing process from string to char**/
	int length = line.size();
	char parse[length+1]={};
	for (int i=0; i<length; i++) parse[i] = line.at(i);

	/** tokenizing process (comma)**/
	char* tmp = strtok(parse, ".");
	int j=0;
	while (tmp != NULL){
		buffer[j] = tmp;
		j++;
		tmp = strtok(NULL,".");
	}

	/** exception (comma in the middle of the words)**/
	int numberofwords=0;

	for (int i=0; i<1000; i++){
		if(buffer[i] == NULL) {numberofwords = i; break;}
		numberofwords = i;
		if(i==999) cout << "***Too many words in a sentence. The result would not be accurate***" << endl;
	}

	for (int i=0; i<numberofwords; i++)	sbuffer[i] = buffer[i];

	for (int i=0; i<numberofwords-1; i++){
		if (isalpha(sbuffer[i].at(sbuffer[i].length()-1)) && isalpha(sbuffer[i+1].at(0))) {

			int start=line.find(sbuffer[i]);
			int end=line.find(sbuffer[i+1]);
			for (int j=start+sbuffer[i].length(); j<end; j++) sbuffer[i]+=line.at(j);
			sbuffer[i]+=sbuffer[i+1];
			sbuffer[i+1]=sbuffer[i];
			sbuffer[i]="";
		}
	}
	/** call the other function to tokenize apostrophe **/
	for(int i=0; i<numberofwords; i++) apostrophe(sbuffer[i]);
}

void apostrophe(string line){
	char *buffer[1000] = {NULL, };
	string sbuffer[1000] = {};
	string line2="";

	/** changing process from string to char**/
	int length = line.size();
	char parse[length+1]={};
	for (int i=0; i<length; i++) parse[i] = line.at(i);

	/** tokenizing process (apostrophe)**/
	char* tmp = strtok(parse, "'");
	int j=0;
	while (tmp != NULL){
		buffer[j] = tmp;
		j++;
		tmp = strtok(NULL,"'");
	}

	/** exception (apostrophe in the middle of the words)**/
	int numberofwords=0;

	for (int i=0; i<1000; i++){
		if(buffer[i] == NULL) {numberofwords = i; break;}
		numberofwords = i;
		if(i==999) cout << "***Too many words in a sentence. The result would not be accurate***" << endl;
	}

	for (int i=0; i<numberofwords; i++)	sbuffer[i] = buffer[i];


	for (int i=0; i<numberofwords-1; i++){
		if (isalpha(sbuffer[i].at(sbuffer[i].length()-1)) && isalpha(sbuffer[i+1].at(0))) {
			int start=line.find(sbuffer[i]);
			int end=line.find(sbuffer[i+1]);
			for (int j=start+sbuffer[i].length(); j<end; j++) sbuffer[i]+=line.at(j);
			sbuffer[i]+=sbuffer[i+1];
			sbuffer[i+1]=sbuffer[i];
			sbuffer[i]="";
		}
	}
	/** call the other function to tokenize remainders **/
	for(int i=0;i<numberofwords;i++) others(sbuffer[i]);
}

void others(string line){
	char *buffer[1000] = {NULL, };
	string sbuffer[1000] = {};
	string line2="";

	/** changing process from string to char**/
	int length = line.size();
	char parse[length+1]={};
	for (int i=0; i<length; i++) parse[i] = line.at(i);

	/** tokenizing process**/
	char* tmp = strtok(parse, " \t\r\n\":,;`()!?{}\\");
	int j=0;
	while (tmp != NULL){
		buffer[j] = tmp;
		j++;
		tmp = strtok(NULL," \t\r\n\":,;`()!?{}\\");
	}

	int numberofwords=0;

	for (int i=0; i<1000; i++){
		if(buffer[i] == NULL) {numberofwords = i; break;}
		numberofwords = i;
		if(i==999) cout << "***Too many words in a sentence. The result would not be accurate***" << endl;
	}

	for (int i=0; i<numberofwords; i++)	sbuffer[i] = buffer[i];

	for (int i=0; i<numberofwords; i++){
		if(sbuffer[i]!="") {
			std::transform(sbuffer[i].begin(), sbuffer[i].end(), sbuffer[i].begin(), ::tolower);
		}
	}
	for (int i=0; i<numberofwords; i++) savetobuffer(sbuffer[i], m);
}

int main(int argc, char**argv) {
	if(argc == 2){
		ifstream book(argv[1]);
		if(book.is_open()){
			string line;
			while(getline(book, line)){
				comma(line);
			}
			book.close();
			multimap<int, string> summary = flipmap(m);
			print(summary);
		}
		else cout << "***Error. Check arguments again.***" << endl;
	}
	else cout << "***Error. Check arguments again.***" << endl;
	return 0;
}
