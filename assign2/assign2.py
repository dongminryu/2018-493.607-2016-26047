import re
import sys
from collections import OrderedDict

if len(sys.argv)==2:

    fileName = sys.argv[1]

    storage = {}

    try:
        with open(fileName, 'r') as txt:
            for i in txt.readlines():
                storage.update({j.lower() : storage.get(j.lower(), 0)+1 for j in re.split('\W+', i)})

        temp = OrderedDict(storage)
        temp = sorted(temp.items(), key=lambda x : x[0])

        for i in sorted(temp, key=lambda x : x[1], reverse=True):
            if i[0]!="": print("{word}: {freq}".format(word=i[0], freq=i[1]))
    except:
        print('Error! Please type the right command.\n(Command: "python assign2.py filename")')
else:
    print('Error! Please type the right command.\n(Command: "python assign2.py filename")')
