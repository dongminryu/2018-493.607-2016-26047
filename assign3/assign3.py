import matplotlib.pyplot as plt
import numpy as np

# Opening result file (top 20)
f = open("output.txt", 'r')

x=[]
y=[]

# Read each line, and split the line by ':' to divide word and frequency
while True:
    line = f.readline()
    if not line: break
    tmp = line.split(':')
    x.append(tmp[0])
    y.append(int(tmp[1][1:-1]))
    
f.close()

# Change the frequency to numpy array to use 'cumsum'
yy = np.array(y)
yyy = np.cumsum(yy)

# Plotting parameters
plt.rcParams["figure.figsize"]=(12, 9)

# PDF plotting
fig, ax1 = plt.subplots()
ax1.set_xlabel('word')
ax1.set_ylabel('frequency', color='b')
ax1.tick_params('y', colors='b')
ax1.plot(x, y, 'b', label='PDF')

# CDF plotting
ax2 = ax1.twinx()
ax2.set_ylabel('cumulated frequency', color='r')
ax2.tick_params('y', colors='r')
ax2.plot(x, yyy, 'r', label='CDF')

# Showing legend
fig.legend(bbox_to_anchor=(0.35, 0., 0.45, 0.5))

# Saving file
plt.savefig('assign3.png')
