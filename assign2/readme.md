# **Assignment 2**  
## **2016-26047**   
## **Dongmin Ryu**  

###  **How to run**  
Python이 설치된 환경에서 아래의 커맨드를 입력합니다.
```
python assign2.py filename
```
제출된 폴더에는 기본적으로 `Aladdin.txt` 파일이 들어 있습니다.

### **version 1. Without exceptions (20181031)**
Split 함수를 이용하여 각 문장별로 tokenization을 진행하였습니다.  
정규표현식 '\W+' (문자가 아닌 것들의 연속)을 기준으로 토큰화하였기 때문에 문자 사이의 ".", "'" 예외를 처리하지 못합니다.

### **version 2. Including exceptions (20181101)**  
위에서 처리할 수 없었던 ".", "'" 예외를 처리할 수 있도록 수정하였습니다.