# Assignment 3  
## 2016-26047 Dongmin Ryu  

**1. Data preprocessing**: 최상위 20개 단어를 crop하여 output.txt 파일을 만들었습니다.  
**2. Graphing**: matplotlib을 사용하여 python 코드로 작성하였습니다.  
**3. Result**: assign3.png 파일로 출력됩니다.
